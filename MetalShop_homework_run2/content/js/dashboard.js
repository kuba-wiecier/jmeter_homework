/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 98.58504187120994, "KoPercent": 1.4149581287900663};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.0700593336376084, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "AddToCart"], "isController": true}, {"data": [0.0, 500, 1500, "SortElements"], "isController": true}, {"data": [0.0, 500, 1500, "LogoutClick"], "isController": false}, {"data": [0.0, 500, 1500, "SortByLatest"], "isController": false}, {"data": [0.20161290322580644, 500, 1500, "SortByLatest-1"], "isController": false}, {"data": [0.2222222222222222, 500, 1500, "RemoveCouponClick"], "isController": false}, {"data": [0.09243697478991597, 500, 1500, "ApplyAndRemoveCoupon"], "isController": true}, {"data": [0.07719298245614035, 500, 1500, "AddToCartClick"], "isController": false}, {"data": [0.02608695652173913, 500, 1500, "RemoveItem"], "isController": true}, {"data": [0.009230769230769232, 500, 1500, "OpenApplication"], "isController": false}, {"data": [0.23893805309734514, 500, 1500, "RemoveFirstClick-0"], "isController": false}, {"data": [0.07407407407407407, 500, 1500, "SearchClick"], "isController": false}, {"data": [0.0967741935483871, 500, 1500, "Search"], "isController": true}, {"data": [0.0, 500, 1500, "Logout"], "isController": true}, {"data": [0.2708333333333333, 500, 1500, "ApplyCouponClick"], "isController": false}, {"data": [0.0, 500, 1500, "LoginClick"], "isController": false}, {"data": [0.12389380530973451, 500, 1500, "RemoveFirstClick-1"], "isController": false}, {"data": [0.03581661891117478, 500, 1500, "MyAccountClick"], "isController": false}, {"data": [0.041254125412541254, 500, 1500, "LoginClick-1"], "isController": false}, {"data": [0.034653465346534656, 500, 1500, "LoginClick-0"], "isController": false}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [0.0743801652892562, 500, 1500, "HomeClick"], "isController": false}, {"data": [0.031914893617021274, 500, 1500, "SortByPriceHighToLow"], "isController": false}, {"data": [0.5212765957446809, 500, 1500, "SortByPriceHighToLow-1"], "isController": false}, {"data": [0.425531914893617, 500, 1500, "SortByPriceHighToLow-0"], "isController": false}, {"data": [0.017543859649122806, 500, 1500, "RemoveFirstClick"], "isController": false}, {"data": [0.20161290322580644, 500, 1500, "SortByLatest-0"], "isController": false}, {"data": [0.3, 500, 1500, "AboutUsClick"], "isController": false}, {"data": [0.07675438596491228, 500, 1500, "CartClick"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3463, 49, 1.4149581287900663, 6748.4016748483955, 0, 65665, 6087.0, 13192.0, 17398.399999999994, 21553.920000000002, 17.75478605047015, 414.8135547992525, 9.300340128097988], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["AddToCart", 256, 1, 0.390625, 17976.753906250004, 2124, 87740, 13959.0, 30925.1, 31157.15, 32015.220000000005, 1.4447360252828805, 153.9749254440729, 2.4814205395186097], "isController": true}, {"data": ["SortElements", 51, 0, 0.0, 6986.6274509803925, 4117, 11025, 6431.0, 10392.2, 10522.4, 11025.0, 0.30968212041169507, 21.983510897698636, 0.8467692583568631], "isController": true}, {"data": ["LogoutClick", 25, 0, 0.0, 4495.880000000001, 3965, 9016, 4296.0, 4889.6, 7780.599999999997, 9016.0, 1.4785025725944765, 35.574562270832104, 0.8110395166775091], "isController": false}, {"data": ["SortByLatest", 62, 0, 0.0, 3789.5806451612902, 1524, 5699, 4617.0, 5314.400000000001, 5476.099999999999, 5699.0, 0.381907442883278, 9.348400983950649, 0.4322325069451717], "isController": false}, {"data": ["SortByLatest-1", 62, 0, 0.0, 2011.2580645161286, 850, 3163, 2437.5, 2978.1000000000004, 3099.15, 3163.0, 0.38321280672476665, 9.198006723607763, 0.21535801617837938], "isController": false}, {"data": ["RemoveCouponClick", 117, 0, 0.0, 3999.4102564102564, 419, 20203, 1732.0, 7922.6, 8139.299999999999, 18139.659999999923, 0.6839785335967917, 0.37353695457096425, 0.46760341338902595], "isController": false}, {"data": ["ApplyAndRemoveCoupon", 119, 0, 0.0, 7098.067226890758, 419, 26561, 3074.0, 14146.0, 14539.0, 24498.59999999997, 0.6810195836051689, 0.8147130453879523, 0.9284547615573029], "isController": true}, {"data": ["AddToCartClick", 285, 0, 0.0, 5453.340350877192, 457, 10315, 5688.0, 9008.0, 9466.5, 10198.96, 1.6765198976440483, 14.497140540898261, 1.1398255739139387], "isController": false}, {"data": ["RemoveItem", 115, 1, 0.8695652173913043, 9060.478260869564, 0, 22370, 4268.0, 17765.4, 18713.59999999996, 22348.56, 0.6719016569679123, 53.20651486253768, 0.7696743029750637], "isController": true}, {"data": ["OpenApplication", 325, 0, 0.0, 8356.353846153848, 1267, 18022, 8824.0, 13350.4, 17118.0, 17815.7, 2.4417364126759926, 58.48957267030172, 0.3671629065491127], "isController": false}, {"data": ["RemoveFirstClick-0", 113, 0, 0.0, 4062.017699115044, 387, 8942, 1737.0, 8150.2, 8400.4, 8929.539999999999, 0.6625545288240536, 0.33715775650534735, 0.4049179740636287], "isController": false}, {"data": ["SearchClick", 27, 0, 0.0, 3580.592592592593, 362, 4916, 3863.0, 4121.4, 4638.799999999998, 4916.0, 0.1695106791727879, 3.22539927220276, 0.09416033748320587], "isController": false}, {"data": ["Search", 31, 0, 0.0, 5186.419354838709, 544, 9436, 5391.0, 7942.0, 8769.399999999998, 9436.0, 0.1927429182521326, 8.909186803638487, 0.20072125099480215], "isController": true}, {"data": ["Logout", 25, 0, 0.0, 8509.319999999998, 7481, 15411, 7998.0, 10345.000000000007, 14513.099999999999, 15411.0, 1.2263317963308153, 50.328896439345634, 1.3585935967085254], "isController": true}, {"data": ["ApplyCouponClick", 120, 0, 0.0, 3156.925, 394, 7953, 1500.5, 6180.1, 6378.9, 7725.779999999992, 0.6870294563879427, 0.45274167692439815, 0.47484872827985003], "isController": false}, {"data": ["LoginClick", 316, 26, 8.227848101265822, 14744.382911392398, 1591, 36065, 16231.0, 21450.2, 21535.8, 34772.06, 2.0425839980349827, 37.28061083401743, 1.4789266617002574], "isController": false}, {"data": ["RemoveFirstClick-1", 113, 0, 0.0, 5158.513274336284, 500, 13741, 2616.0, 9750.0, 10958.29999999999, 13698.72, 0.6631066251980517, 53.09505154847133, 0.367788302843143], "isController": false}, {"data": ["MyAccountClick", 349, 21, 6.017191977077364, 6045.323782234958, 611, 10581, 6425.0, 9300.0, 9446.0, 9634.0, 2.5549422392714387, 42.87304404392817, 0.569517283378966], "isController": false}, {"data": ["LoginClick-1", 303, 0, 0.0, 8137.306930693063, 866, 13217, 8525.0, 12804.0, 12951.8, 13173.279999999999, 1.9676346823211595, 34.071606854982726, 0.6937124777585849], "isController": false}, {"data": ["LoginClick-0", 303, 0, 0.0, 7045.092409240926, 663, 28060, 7769.0, 8758.4, 9022.8, 27847.52, 1.975885072612146, 1.9488837553554312, 0.7310670329607628], "isController": false}, {"data": ["Login", 322, 32, 9.937888198757763, 20684.534161490697, 2264, 44044, 23042.5, 30649.7, 30887.5, 43746.02, 2.072058738360757, 71.84821948539584, 1.8757294636134902], "isController": true}, {"data": ["HomeClick", 363, 0, 0.0, 7020.002754820933, 400, 59852, 6168.0, 13133.400000000001, 13399.4, 31978.72, 1.940927367610574, 46.707893480582705, 1.0520510151771965], "isController": false}, {"data": ["SortByPriceHighToLow", 47, 0, 0.0, 2127.1063829787236, 1201, 3489, 2006.0, 2998.2000000000003, 3275.399999999999, 3489.0, 0.29003751974723535, 7.076130847418666, 0.33157131683204977], "isController": false}, {"data": ["SortByPriceHighToLow-1", 47, 0, 0.0, 1012.9361702127663, 426, 1361, 1035.0, 1218.4, 1245.6, 1361.0, 0.2914819064157028, 6.99044810498, 0.16547266969518434], "isController": false}, {"data": ["SortByPriceHighToLow-0", 47, 0, 0.0, 1114.191489361702, 714, 2296, 964.0, 1947.4, 2192.799999999999, 2296.0, 0.29087218333611825, 0.12066892479406868, 0.1673989722000458], "isController": false}, {"data": ["RemoveFirstClick", 114, 1, 0.8771929824561403, 9139.956140350878, 0, 22370, 4273.0, 17766.0, 18898.25, 22349.899999999998, 0.666339342077576, 53.22890660618468, 0.7699982172500058], "isController": false}, {"data": ["SortByLatest-0", 62, 0, 0.0, 1777.6774193548385, 553, 2803, 2083.5, 2582.2, 2734.85, 2803.0, 0.3861484803188839, 0.18374410267189836, 0.22002459166355257], "isController": false}, {"data": ["AboutUsClick", 35, 0, 0.0, 1899.0571428571436, 496, 4571, 1485.0, 4410.6, 4530.2, 4571.0, 0.2177388750987601, 6.456935770531221, 0.12143316505228843], "isController": false}, {"data": ["CartClick", 228, 1, 0.43859649122807015, 5728.653508771931, 481, 65665, 4961.0, 9424.0, 9678.65, 9992.07, 1.3026263918962926, 108.07423153791328, 0.7235548631385297], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user22&lt;\\\/strong&gt;\\\/", 2, 4.081632653061225, 0.057753393011839446], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user29&lt;\\\/strong&gt;\\\/", 3, 6.122448979591836, 0.08663008951775916], "isController": false}, {"data": ["Non HTTP response code: java.net.URISyntaxException\/Non HTTP response message: Illegal character in query at index 41: https:\\\/\\\/lifesciencejobs.pl\\\/?remove_item=${elemToRemove}&amp;_wpnonce=${_wpnonce}", 1, 2.0408163265306123, 0.028876696505919723], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user26&lt;\\\/strong&gt;\\\/", 2, 4.081632653061225, 0.057753393011839446], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user28&lt;\\\/strong&gt;\\\/", 2, 4.081632653061225, 0.057753393011839446], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user23&lt;\\\/strong&gt;\\\/", 2, 4.081632653061225, 0.057753393011839446], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Apply coupon\\\/", 1, 2.0408163265306123, 0.028876696505919723], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user25&lt;\\\/strong&gt;\\\/", 3, 6.122448979591836, 0.08663008951775916], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Username or email address\\\/", 21, 42.857142857142854, 0.6064106266243142], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user30&lt;\\\/strong&gt;\\\/", 4, 8.16326530612245, 0.11550678602367889], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user27&lt;\\\/strong&gt;\\\/", 3, 6.122448979591836, 0.08663008951775916], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user24&lt;\\\/strong&gt;\\\/", 4, 8.16326530612245, 0.11550678602367889], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user21&lt;\\\/strong&gt;\\\/", 1, 2.0408163265306123, 0.028876696505919723], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3463, 49, "Test failed: text expected to contain \\\/Username or email address\\\/", 21, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user30&lt;\\\/strong&gt;\\\/", 4, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user24&lt;\\\/strong&gt;\\\/", 4, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user29&lt;\\\/strong&gt;\\\/", 3, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user25&lt;\\\/strong&gt;\\\/", 3], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["LoginClick", 316, 26, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user30&lt;\\\/strong&gt;\\\/", 4, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user24&lt;\\\/strong&gt;\\\/", 4, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user29&lt;\\\/strong&gt;\\\/", 3, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user27&lt;\\\/strong&gt;\\\/", 3, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user25&lt;\\\/strong&gt;\\\/", 3], "isController": false}, {"data": [], "isController": false}, {"data": ["MyAccountClick", 349, 21, "Test failed: text expected to contain \\\/Username or email address\\\/", 21, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["RemoveFirstClick", 114, 1, "Non HTTP response code: java.net.URISyntaxException\/Non HTTP response message: Illegal character in query at index 41: https:\\\/\\\/lifesciencejobs.pl\\\/?remove_item=${elemToRemove}&amp;_wpnonce=${_wpnonce}", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["CartClick", 228, 1, "Test failed: text expected to contain \\\/Apply coupon\\\/", 1, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
